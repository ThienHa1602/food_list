// import { MonAn } from "../../models/v1/model-v1.js";

export let layThongTinTuForm = () => {
  let id = document.getElementById("foodID").value;
  let name = document.getElementById("tenMon").value;
  let type = document.getElementById("loai").value;
  let price = document.getElementById("giaMon").value;
  let discount = document.getElementById("khuyenMai").value;
  let status = document.getElementById("tinhTrang").value;
  let img = document.getElementById("hinhMon").value;
  let desc = document.getElementById("moTa").value;
  return {
    id,
    name,
    type,
    price,
    discount,
    status,
    img,
    desc,
  };
  // return new MonAn(ma, ten, loai, gia, khuyenMai, tinhTrang, hinhMon, moTa);
};
