export let renderFoodList = (foodArr) => {
  let contentHTML = "";
  foodArr.forEach((item) => {
    let contentTr = `
    <tr>
<td>${item.id}</td>
<td>${item.name}</td>
<td>${item.type ? "Chay" : "Mặn"}</td>
<td>${item.price}</td>
<td>${item.discount}</td>
<td>0</td>
<td>${item.status ? "Còn" : "Hết"}</td>
<td><button onclick="deleteFood(${item.id})" class="btn btn-danger">Xóa</button>
    <button onclick="editFood(${
      item.id
    })" class="btn btn-success">Sửa</button></td>
   </tr> `;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};
export let onSuccess = (message) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "center", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
export let hienThiThongTin = (item) => {
  document.getElementById("foodID").value = item.id;
  document.getElementById("tenMon").value = item.name;
  document.getElementById("loai").value = item.type ? "loai1" : "loai2";
  document.getElementById("giaMon").value = item.price;
  document.getElementById("khuyenMai").value = item.discount;
  document.getElementById("tinhTrang").value = item.status ? "0" : "1";
  document.getElementById("hinhMon").value = item.img;
  document.getElementById("moTa").value = item.desc;
};
