import { layThongTinTuForm } from "../v1/controller-v1.js";
import { hienThiThongTin, onSuccess, renderFoodList } from "./controller-v2.js";

const BASE_URL = "https://63f85e4e5b0e4a127de45517.mockapi.io";
// lấy dữ liệu từ sever
let fetchFoodList = () => {
  axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      renderFoodList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
// render dữ liệu ra màn hình
fetchFoodList();
let deleteFood = (id) => {
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res);
      onSuccess("Xóa thành công");
      fetchFoodList();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deleteFood = deleteFood;

window.createFood = () => {
  let data = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/food`,
    method: "POST",
    data,
  })
    .then((res) => {
      console.log(res.data);
      onSuccess("Thêm thành công");
      $("#exampleModal").modal("hide");
      fetchFoodList();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.editFood = (id) => {
  $("#exampleModal").modal("show");
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "GET",
  })
    .then((res) => {
      hienThiThongTin(res.data);
      console.log(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.capNhatMon = () => {
  let data = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/food/${data.id}`,
    method: "PUT",
    data,
  })
    .then((res) => {
      console.log(res.data);
      onSuccess("Cập nhật thành công");
      $("#exampleModal").modal("hide");
      fetchFoodList();
    })
    .catch((err) => {
      console.log(err);
    });
};
